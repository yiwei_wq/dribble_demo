//
//  ConstraintsHelpers.swift
//  Dribble-demo
//
//  Created by yiwei on 14-12-2.
//  Copyright (c) 2014年 com.nerdydoes. All rights reserved.
//

import Foundation
import UIKit

func resetViewConstraints(view: UIView!, superView: UIView! = nil) {
    if superView != nil {
        superView.setTranslatesAutoresizingMaskIntoConstraints(false)
        superView.removeConstraints(superView.constraints())
    }
    view.setTranslatesAutoresizingMaskIntoConstraints(false)
    view.removeConstraints(view.constraints())
}

func addVisualConstraints(
    formatString: String, #toView: UIView!, #withNameBindings: NSDictionary!) {
    toView.addConstraints(NSLayoutConstraint.constraintsWithVisualFormat(formatString, options: nil, metrics: nil, views: withNameBindings))
}

func relate(item: UIView!, #attribute: NSLayoutAttribute!, #toView: UIView!, #toAttribute: NSLayoutAttribute!, relation: NSLayoutRelation! = .Equal, multiplier: CGFloat! = 1.0, constant: CGFloat! = 0.0) {
    
    let constraint = NSLayoutConstraint(item: item, attribute: attribute, relatedBy: relation, toItem: toView, attribute: toAttribute, multiplier: multiplier, constant: constant)
    
    toView.addConstraint(constraint)
}