//
//  ViewController.swift
//  Dribble-demo
//
//  Created by yiwei on 14-11-28.
//  Copyright (c) 2014年 com.nerdydoes. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    @IBOutlet weak var backgroundImage: UIImageView!
    
    @IBOutlet weak var dialogView: UIView!
    @IBOutlet weak var detailButton: UIButton!
    @IBOutlet weak var dialogHeaderView: UIView!
    @IBOutlet weak var dialogLikesButton: UIButton!
    @IBOutlet weak var dialogLikeLabel: UILabel!
    @IBOutlet weak var dialogAvatarImage: UIImageView!
    @IBOutlet weak var dialogIntroLabel: UILabel!
    @IBOutlet weak var dialogTitleLabel: UILabel!
    
    @IBOutlet weak var likebutton: UIButton!
    @IBOutlet weak var shareButton: UIButton!
    
    @IBOutlet weak var profileButton: UIButton!
    
    @IBOutlet weak var profilePopoverView: UIView!
    @IBOutlet weak var profilePopoverBackgroundImage: UIImageView!
    @IBOutlet weak var popoverProfileButton: UIButton!
    @IBOutlet weak var popoverFavoritesButton: UIButton!
    @IBOutlet weak var popoverShareButton: UIButton!
    
    @IBOutlet weak var shareView: UIView!
    @IBOutlet weak var shareViewBackground: UIImageView!
    @IBOutlet weak var shareLabel: UILabel!
    @IBOutlet weak var shareViewEmailButton: UIButton!
    @IBOutlet weak var shareViewEmailLabel: UILabel!
    @IBOutlet weak var shareViewTwitterButton: UIButton!
    @IBOutlet weak var shareViewTwitterlabel: UILabel!
    @IBOutlet weak var shareViewFacebookButton: UIButton!
    @IBOutlet weak var shareViewFacebookLabel: UILabel!
    
    @IBOutlet weak var maskButton: UIButton!
    
    @IBOutlet var PanRecognizer: UIPanGestureRecognizer!
    
    var animator : UIDynamicAnimator!
    var attachmentBehavior : UIAttachmentBehavior!
    var gravityBehaviour : UIGravityBehavior!
    var snapBehavior : UISnapBehavior!
    
    private let nameBindings = NSMutableDictionary()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpInitialNameBindings()
        setUpInitialConstraints()
        
        animateDialogView()
        
        animator = UIDynamicAnimator(referenceView: view)
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        self.view.layoutIfNeeded()
        insertBlurView(self.backgroundImage, UIBlurEffectStyle.Dark)
        insertBlurView(self.dialogHeaderView, UIBlurEffectStyle.Dark)
    }
    
    @IBAction func detailButtonPressed(sender: AnyObject) {
        resetViewConstraints(self.dialogView, superView: self.view)
        addVisualConstraints("V:|-0-[dialogView(300)]", toView: self.view, withNameBindings: nameBindings)
        addVisualConstraints("|-0-[dialogView]-0-|", toView: self.view, withNameBindings: nameBindings)
        addVisualConstraints("|-0-[detailButton]-0-|", toView: self.dialogView, withNameBindings: nameBindings)
        addVisualConstraints("V:[detailButton(300)]", toView: self.detailButton, withNameBindings: nameBindings)
        addVisualConstraints("V:|-0-[dialogHeaderView(60)]", toView: self.dialogView, withNameBindings: nameBindings)
        addVisualConstraints("|-0-[dialogHeaderView]-0-|", toView: self.dialogView, withNameBindings: nameBindings)
        self.dialogView.layer.cornerRadius = 0

        scaleFillBackgroundImage()
        
        UIView.animateWithDuration(0.3, animations: {
            self.view.layoutIfNeeded()
            self.updateBlurViews()
            self.detailButton.userInteractionEnabled = false
        })
        
    }
    
    private func showMask() {
        maskButton.hidden = false
        resetViewConstraints(self.maskButton)
        addVisualConstraints("|-0-[maskButton]-0-|", toView: self.view, withNameBindings: nameBindings)
        addVisualConstraints("V:|-[maskButton]-|", toView: self.view, withNameBindings: nameBindings)
        
        self.maskButton.alpha = 0
        spring(0.3, {
            self.maskButton.alpha = 0.5
        })
    }
    
    @IBAction func maskButtonPressed(sender: AnyObject) {
        spring(0.5) {
            self.maskButton.alpha = 0
        }
        
        hidePopover()
    }
}


extension ViewController {
    private func setUpInitialNameBindings() {
        self.nameBindings.setValue(self.backgroundImage, forKey: "backgroundImage")
        
        self.nameBindings.setValue(dialogView, forKey: "dialogView")
        self.nameBindings.setValue(dialogHeaderView, forKey: "dialogHeaderView")
        self.nameBindings.setValue(dialogLikeLabel, forKey: "dialogLikeLabel")
        self.nameBindings.setValue(dialogLikesButton, forKey: "dialogLikesButton")
        self.nameBindings.setValue(detailButton, forKey: "detailButton")
        self.nameBindings.setValue(dialogAvatarImage, forKey: "dialogAvatarImage")
        self.nameBindings.setValue(dialogIntroLabel, forKey: "dialogIntroLabel")
        self.nameBindings.setValue(dialogTitleLabel, forKey: "dialogTitleLabel")
        self.nameBindings.setValue(likebutton, forKey: "likeButton")
        self.nameBindings.setValue(shareButton, forKey: "shareButton")
        
        self.nameBindings.setValue(profileButton, forKey: "profileButton")
        self.nameBindings.setValue(profilePopoverView, forKey: "profilePopoverView")
        self.nameBindings.setValue(profilePopoverBackgroundImage, forKey: "profilePopoverBackgroundImage")
        self.nameBindings.setValue(popoverProfileButton, forKey: "popoverProfileButton")
        self.nameBindings.setValue(popoverFavoritesButton, forKey: "popoverFavoritesButton")
        self.nameBindings.setValue(popoverShareButton, forKey: "popoverShareButton")
        
        self.nameBindings.setValue(shareView, forKey: "shareView")
        self.nameBindings.setValue(shareViewBackground, forKey: "shareViewBackground")
        self.nameBindings.setValue(shareLabel, forKey: "shareLabel")
        self.nameBindings.setValue(shareViewEmailButton, forKey: "shareViewEmailButton")
        self.nameBindings.setValue(shareViewEmailLabel, forKey: "shareViewEmailLabel")
        self.nameBindings.setValue(shareViewTwitterButton, forKey: "shareViewTwitterButton")
        self.nameBindings.setValue(shareViewTwitterlabel, forKey: "shareViewTwitterLabel")
        self.nameBindings.setValue(shareViewFacebookButton, forKey: "shareViewFacebookButton")
        self.nameBindings.setValue(shareViewFacebookLabel, forKey: "shareViewFacebookLabel")
        
        self.nameBindings.setValue(maskButton, forKey: "maskButton")
    }
    
    
    private func setUpInitialConstraints() {
        resetViewConstraints(self.view)
        resetViewConstraints(self.backgroundImage)
        
        scaleFillBackgroundImage()
        setupDialogView()
        
        self.view.layoutIfNeeded()
    }
    
    private func scaleFillBackgroundImage() {
        self.view.addConstraints(NSLayoutConstraint.constraintsWithVisualFormat("|-0-[backgroundImage]-0-|", options: nil, metrics: nil, views:self.nameBindings))
        self.view.addConstraints(NSLayoutConstraint.constraintsWithVisualFormat("V:|-0-[backgroundImage]-0-|", options: nil, metrics: nil, views: nameBindings))
    }
    
    private func setupDialogView() {
        // Centralize the 300x300 dialog
        addVisualConstraints("[dialogView(300)]", toView: self.view, withNameBindings: nameBindings)
        addVisualConstraints("V:[dialogView(300)]", toView: self.view, withNameBindings: nameBindings)
        relate(self.dialogView, attribute: .CenterX, toView: self.view, toAttribute: .CenterX)
        relate(self.dialogView, attribute: .CenterY, toView: self.view, toAttribute: .CenterY)

        fillDialogView()
        setUpProfileView()
        
        self.view.layoutIfNeeded()
    }
    
    private func fillDialogView() {
        // Header view
        resetViewConstraints(self.dialogHeaderView)
        addVisualConstraints("V:[dialogHeaderView(60)]", toView: self.dialogView, withNameBindings: nameBindings)
        addVisualConstraints("|-0-[dialogHeaderView]-0-|", toView: self.dialogView, withNameBindings: nameBindings)
        
        
        // Title in HeaderView
        resetViewConstraints(self.dialogTitleLabel)
        addVisualConstraints("V:|-[dialogTitleLabel]", toView: self.dialogHeaderView, withNameBindings: nameBindings)
        relate(dialogIntroLabel, attribute: .CenterX, toView: self.dialogHeaderView, toAttribute: .CenterX)
        
        // Intro in HeaderView
        resetViewConstraints(self.dialogIntroLabel)
        addVisualConstraints("V:|-[dialogTitleLabel]-[dialogIntroLabel]", toView: self.dialogHeaderView, withNameBindings: nameBindings)
        relate(dialogIntroLabel, attribute: .CenterX, toView: dialogHeaderView, toAttribute: .CenterX)
        
        // Avatar in HeaderView
        resetViewConstraints(self.dialogAvatarImage)
        addVisualConstraints("[dialogAvatarImage(20)]", toView: self.dialogAvatarImage, withNameBindings: nameBindings)
        addVisualConstraints("V:[dialogAvatarImage(20)]", toView: self.dialogAvatarImage, withNameBindings: nameBindings)
        addVisualConstraints("[dialogAvatarImage]-[dialogIntroLabel]", toView: self.dialogHeaderView, withNameBindings: nameBindings)
        
        // Likes button in HeaderView
        resetViewConstraints(self.dialogLikesButton)
        addVisualConstraints("[dialogLikesButton(40)]", toView: self.dialogLikesButton, withNameBindings: nameBindings)
        addVisualConstraints("V:[dialogLikesButton(40)]", toView: self.dialogLikesButton, withNameBindings: nameBindings)
        addVisualConstraints("[dialogLikesButton]-|", toView: self.dialogHeaderView, withNameBindings: nameBindings)
        addVisualConstraints("V:|-[dialogLikesButton]", toView: self.dialogHeaderView, withNameBindings: nameBindings)
        
        // Likes label in HeaderView
        resetViewConstraints(self.dialogLikeLabel)
        relate(self.dialogLikeLabel, attribute: .CenterX, toView: self.dialogHeaderView, toAttribute: .CenterX)
        addVisualConstraints("V:|-13-[dialogLikeLabel]", toView: self.dialogHeaderView, withNameBindings: nameBindings)
    }
    
    private func updateBlurViews() {
        let blurBackground: UIVisualEffectView = self.backgroundImage.subviews[0] as UIVisualEffectView
        blurBackground.removeFromSuperview()
        let blurHeader: UIVisualEffectView = self.dialogHeaderView.subviews[0] as UIVisualEffectView
        blurHeader.removeFromSuperview()
        insertBlurView(self.backgroundImage, UIBlurEffectStyle.Light)
        insertBlurView(self.dialogHeaderView, UIBlurEffectStyle.ExtraLight)
    }
}

extension ViewController {
    
    @IBAction func profilePressed(sender: AnyObject) {
        showMask()
        
        self.profilePopoverView.hidden = false
        
        let scale = CGAffineTransformMakeScale(0.3, 0.3)
        let translation = CGAffineTransformMakeTranslation(50, -50)
        self.profilePopoverView.transform = CGAffineTransformConcat(scale, translation)
        self.profilePopoverView.alpha = 0
        
        UIView.animateWithDuration(0.3, delay: 0.0, usingSpringWithDamping: 0.7, initialSpringVelocity: 0.7, options: nil, animations: {
            let scale = CGAffineTransformMakeScale(1, 1)
            let translation = CGAffineTransformMakeTranslation(0, 0)
            self.profilePopoverView.transform = CGAffineTransformConcat(scale, translation)
            self.profilePopoverView.alpha = 1
        }, completion: nil)
    }
    
    private func setUpProfileView() {
        setUpProfileButton()
        setUpProfileBackground()
        fillProfileButtons()
    }
    
    private func setUpProfileButton() {
        resetViewConstraints(self.profileButton)
        addVisualConstraints("V:|-28-[profileButton]", toView: self.view, withNameBindings: nameBindings)
        addVisualConstraints("[profileButton]-|", toView: self.view, withNameBindings: nameBindings)
    }
    
    private func setUpProfileBackground() {
        //Profile popover view
        resetViewConstraints(self.profilePopoverView)
        addVisualConstraints("[profilePopoverView(205)]", toView: self.view, withNameBindings: nameBindings)
        addVisualConstraints("V:[profilePopoverView(168)]", toView: self.view, withNameBindings: nameBindings)
        addVisualConstraints("[profilePopoverView]-|", toView: self.view, withNameBindings: nameBindings)
        addVisualConstraints("V:[profileButton]-[profilePopoverView]", toView: self.view, withNameBindings: nameBindings)
        
        //Profile popover background image
        resetViewConstraints(self.profilePopoverBackgroundImage)
        addVisualConstraints("|-[profilePopoverBackgroundImage]-|", toView: self.profilePopoverView, withNameBindings: nameBindings)
        addVisualConstraints("V:|-[profilePopoverBackgroundImage]-|", toView: self.profilePopoverView, withNameBindings: nameBindings)
    }
    
    private func fillProfileButtons() {
        addVisualConstraints("|-8-[popoverProfileButton(205)]", toView: self.profilePopoverView, withNameBindings: nameBindings)
        addVisualConstraints("V:|-20-[popoverProfileButton(42)]", toView: self.profilePopoverView, withNameBindings: nameBindings)
        addVisualConstraints("|-8-[popoverFavoritesButton(205)]", toView: self.profilePopoverView, withNameBindings: nameBindings)
        addVisualConstraints("V:|-70-[popoverFavoritesButton(42)]", toView: self.profilePopoverView, withNameBindings: nameBindings)
        addVisualConstraints("|-8-[popoverShareButton(205)]", toView: self.profilePopoverView, withNameBindings: nameBindings)
        addVisualConstraints("V:|-120-[popoverShareButton(42)]", toView: self.profilePopoverView, withNameBindings: nameBindings)
    }
    
    private func hidePopover() {
        spring(0.5) {
            self.profilePopoverView.alpha = 0
        }
    }
}

extension ViewController {
    private func animateDialogView() {
        let scale = CGAffineTransformMakeScale(0.5, 0.5)
        let translation = CGAffineTransformMakeTranslation(0, -200)
        self.dialogView.transform = CGAffineTransformConcat(scale, translation)
        
        UIView.animateWithDuration(0.5, delay: 0.1, usingSpringWithDamping: 0.7, initialSpringVelocity: 0.7, options: nil, animations: {
            let scale = CGAffineTransformMakeScale(1, 1)
            let translation = CGAffineTransformMakeTranslation(0, 0)
            self.dialogView.transform = CGAffineTransformConcat(scale, translation)
        }, completion: nil)
    }
}

extension ViewController {
    @IBAction func HandleGesture(sender: AnyObject) {
        let myView = self.dialogView
        let location = sender.locationInView(view)
        let boxLocation = sender.locationInView(dialogView)
        
        if sender.state == UIGestureRecognizerState.Began {
            animator.removeBehavior(snapBehavior)
            
            let centerOffset = UIOffsetMake(boxLocation.x - CGRectGetMidX(myView.bounds), boxLocation.y - CGRectGetMidY(myView.bounds));
            attachmentBehavior = UIAttachmentBehavior(item: myView, offsetFromCenter: centerOffset, attachedToAnchor: location)
            attachmentBehavior.frequency = 0
            
            animator.addBehavior(attachmentBehavior)
        }
        else if sender.state == UIGestureRecognizerState.Changed {
            attachmentBehavior.anchorPoint = location
        }
        else if sender.state == UIGestureRecognizerState.Ended {
            animator.removeBehavior(attachmentBehavior)
            snapBehavior = UISnapBehavior(item: myView, snapToPoint: view.center)
            animator.addBehavior(snapBehavior)
            
            let translation = sender.translationInView(view)
            if translation.y > 100 {
                animator.removeAllBehaviors()
                
                var gravity = UIGravityBehavior(items: [dialogView])
                gravity.gravityDirection = CGVectorMake(0, 10)
                animator.addBehavior(gravity)
            }
        }
    }
}
